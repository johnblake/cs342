//
//  WebView.swift
//  MovieDatabase
//
//  Created by Cody Bohlman on 4/18/15.
//  Copyright (c) 2015 Carleton. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak varbusyindicator:UIActivityIndicatorView!
    
    @IBOutlet weak varmyWebView: UIWebView!
    //code to do navigation in the web view
    @IBActionfunc back(sender:AnyObject) {
    if myWebView.canGoBack{
    myWebView.goBack()
    }
    }
    
    @IBActionfunc forward(sender:AnyObject) {
    if myWebView.canGoForward{
    myWebView.goForward()
    }
    }
    // gotoa internal or external html page
    @IBActionfuncmakeAppPie(sender:AnyObject) {
    leturl =NSURL(string: "http://makeapppie.com")
    letrequestObj =NSURLRequest(URL:url!)
    myWebView.loadRequest(requestObj)
    }
    
    @IBActionfunchelloPizza(sender:UIBarButtonItem) {
    letmyURL = NSBundle.mainBundle().URLForResource("myHtml",withExtension: "html")
    letrequestObj =NSURLRequest(URL:myURL!)
    myWebView.loadRequest(requestObj)
    }
    
    overridefuncviewDidLoad() {
    super.viewDidLoad()
    //letmyHTMLString:String! = "</pre>
    <h1>Hello Pizza!</h1>
    <pre>"
    letmyHTMLString:String! = "</pre>
    <h1 style="0font-family: Helvetica\&quot;;">Hello Pizza</h1>
    Tap the buttons above to see <strong>some cool stuff</strong> with <code>UIWebView</code>
    
    <img src="\&quot;https://apppie.files.wordpress.com/2014/09/photo-sep-14-7-40-59-pm_small1.jpg\&quot;" alt="" />"
    myWebView.loadHTMLString(myHTMLString, baseURL: nil)
}

}
