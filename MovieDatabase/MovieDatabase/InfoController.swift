
//
//  ViewController.swift
//  MovieDatabase
//
//  Created by John Blake on 4/15/15.
//  Copyright (c) 2015 Carleton. All rights reserved.
//

import UIKit


class InfoController: UIViewController  {
    
    @IBOutlet weak var moviePoster : UIImageView?
    
    @IBOutlet weak var movieTitle : UILabel?
    
    @IBOutlet weak var movieSummary: UILabel?
    
    var mPoster: String?
    var mTitle: String?
    var mSummary: String?
    
    @IBOutlet weak var picker: UIPickerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setValues()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        self.movieTitle!.text = self.mTitle!
        self.movieSummary!.text = self.mSummary!
        
        moviePoster!.image = UIImage(named: mPoster!)
    }
    
    
    
}

