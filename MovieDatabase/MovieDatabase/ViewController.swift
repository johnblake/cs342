//
//  ViewController.swift
//  MovieDatabase
//
//  Created by John Blake on 4/15/15.
//  Copyright (c) 2015 Carleton. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {

    // The following String arrays contain the textual info about the movies, in order (the indices line up)
    
    var movieTitles = ["Do You Believe?", "Cinderella", "Get Hard", "The Gunman", "Home", "Insurgent", "It Follows", "Kingsman: The Secret Service", "The Second Best Marigold Hotel", "Run All Night"]
    
    var movieReviews = ["Do You Believe?":"Wherever you fall on the spectrum of spirituality, the only answer to \"Do You Believe?\" is \"No.\"",
        "Cinderella":"Based on the classic fairy tale, but borrowing heavily from the 1950 film, Cinderella is enchanting, a wonderful and stylish film with a charming lead and emotional narrative.",
        "Get Hard":"Dumb, juvenile comedy has its place when it\'s funny. Unfortunately, too often in Get Hard, it\'s not.",
        "The Gunman":"A dull, generic retread, made far worse by Penn\'s self-seriousness as an actor, by the banal political pieties he\'s grafted on as producer and co-writer, and by the presence of a pitifully retrograde female lead role.",
        "Home":"\"Home\" is a fine selection for a second quarter film and a great choice for your children.",
        "Insurgent":"I appreciate the attempt to try to make this world -- an obliterated, totalitarian Chicago -- as watchable as possible. But there\'s too much to keep straight.",
        "It Follows":"The next time some old lady starts following you with an emotionless stare on her face, you are going to lose your mind.",
        "Kingsman: The Secret Service":"One of the most smartly-crafted action films of recent years... [but] it has some grim, grim thoughts about human beings and society.",
        "The Second Best Marigold Hotel":"There is way too much going on. Parallel stories for all of these characters mean most all of them aren\'t fully developed.",
        "Run All Night":"Run All Night is a taut, edgy affair that features Neeson in peak action form and allows him to partially atone for the indignity of Taken 3."]
    
    var movieSummaries = ["Do You Believe?":"When a pastor is shaken by the visible faith of a street-corner preacher, he is reminded that true belief always requires action. His response ignites a journey that impacts everyone it touches in ways that only God could orchestrate.",
        "Cinderella":"When her father unexpectedly passes away, young Ella finds herself at the mercy of her cruel stepmother and her daughters. Never one to give up hope, Ella\'s fortunes begin to change after meeting a dashing stranger.",
        "Get Hard":"When millionaire James King is nailed for fraud and bound for San Quentin, he turns to Darnell Lewis to prep him to go behind bars.",
        "The Gunman":"A sniper on a mercenary assassination team, kills the minister of mines of the Congo. Terrier\'s successful kill shot forces him into hiding. Returning to the Congo years later, he becomes the target of a hit squad himself.",
        "Home":"Oh, an alien on the run from his own people, lands on Earth and makes friends with the adventurous Tip, who is on a quest of her own.",
        "Insurgent":"Beatrice Prior must confront her inner demons and continue her fight against a powerful alliance which threatens to tear her society apart with the help from others on her side.",
        "It Follows":"A young woman is followed by an unknown supernatural force after getting involved in a sexual confrontation.",
        "Kingsman: The Secret Service":"A spy organization recruits an unrefined, but promising street kid into the agency\'s ultra-competitive training program, just as a global threat emerges from a twisted tech genius.",
        "The Second Best Marigold Hotel":"As the Best Exotic Marigold Hotel has only a single remaining vacancy - posing a rooming predicament for two fresh arrivals - Sonny pursues his expansionist dream of opening a second hotel.",
        "Run All Night":"Mobster and hit man Jimmy Conlon has one night to figure out where his loyalties lie: with his estranged son, Mike, whose life is in danger, or his longtime best friend, mob boss Shawn Maguire, who wants Mike to pay for the death of his own son."]
    
    var moviePosters = ["Do You Believe?":"believe.jpg", "Cinderella":"cinderella.jpg", "Get Hard":"gethard.jpg", "The Gunman":"gunman.jpg", "Home":"home.jpg", "Insurgent":"insurgent.jpg", "It Follows":"itfollows.jpg", "Kingsman: The Secret Service":"kingsman.jpg", "The Second Best Marigold Hotel":"marigold.jpg", "Run All Night":"run.jpg"]
    
    var movieRatings = ["Do You Believe?": 54 , "Cinderella": 76, "Get Hard": 63, "The Gunman": 56, "Home": 68, "Insurgent": 70, "It Follows": 76, "Kingsman: The Secret Service": 82, "The Second Best Marigold Hotel": 68, "Run All Night": 71]
    
    // These options are displayed in the list view
    var options = ["Information", "Trailer", "Reviews"]
    
    
    //These IBOutlets represent the pickerview, tableview, and button
    @IBOutlet weak var picker: UIPickerView?
    
    @IBOutlet weak var table: UITableView?
    
    //@IBOutlet weak var button: UIButton?
    
    
    //This is instantiated to keep track of what option is selected
    var selected: String?
    
    //These are instantiated to store the values from the movie selected in Picker
    var mTitle: String?
    var mSummary: String?
    var mPoster: String?
    var mReview: String?
    var mRating: Int?
    
    let cellIdentifier = "OptionsCell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.table?.delegate = self
        self.table?.dataSource = self
        self.selected? = ""
        self.picker?.delegate = self
        self.picker?.dataSource = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return movieTitles.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return movieTitles[row]
    }
    
    //Upon selecting an item in the pickerview, we will set the values of the fields defined above to be the
    //correct title, summary, and name of the image containing the correct movie poster (in the images file)
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mTitle = movieTitles[row]
        self.mSummary = movieSummaries[self.mTitle!]
        self.mPoster = moviePosters[self.mTitle!]
        self.mReview = movieReviews[self.mTitle!]
        println(self.mTitle)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    
    //Upon selecting an option in the table view, this function will navigate to the correct view, based on the 
    //selection, and pass the data specified by the movie chosen in the picker
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        

        cell.textLabel?.text = self.options[indexPath.row]
        
        self.selected = self.options[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.None
        return indexPath
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        
        self.selected = options[indexPath.row]
        
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        println("SELECTED==========" + self.selected!)
        if self.mTitle == nil {
            self.mTitle = movieTitles[0]
            self.mReview = movieReviews[self.mTitle!]
            self.mSummary = movieSummaries[self.mTitle!]
            self.mPoster = moviePosters[self.mTitle!]
            self.mRating = movieRatings[self.mTitle!]
            println("NIL")
        }
        
        if self.selected! == "Information" {
            let infoVC = storyboard.instantiateViewControllerWithIdentifier("Information") as! InfoController
            infoVC.mPoster = self.mPoster
            infoVC.mSummary = self.mSummary
            infoVC.mTitle = self.mTitle
            self.navigationController?.pushViewController(infoVC, animated: true)
            //self.presentViewController(infoVC, animated:false, completion:nil)
        }
            
        else if self.selected! == "Trailer" {
            let infoVC = storyboard.instantiateViewControllerWithIdentifier("Trailer") as! TrailerController
            infoVC.mTitle = self.mTitle
            infoVC.cleanTitle(infoVC.mTitle)
            self.navigationController?.pushViewController(infoVC, animated: true)
            //self.presentViewController(infoVC, animated:false, completion:nil)
        }
            
        else if self.selected! == "Reviews" {
            let reviewVC = storyboard.instantiateViewControllerWithIdentifier("Review") as! ReviewController
            println(mTitle)
            reviewVC.mTitle = self.mTitle
            reviewVC.mReview = self.mReview
            reviewVC.mRating = self.mRating
            self.navigationController?.pushViewController(reviewVC, animated: true)
            //self.presentViewController(infoVC, animated:false, completion:nil)
        }
    }
    
    /*override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier != nil && segue.identifier! == "infosegue" {
            var titleVC = segue.destinationViewController as InfoController
            
            titleVC.mPoster = self.mPoster
            titleVC.mSummary = self.mSummary
            titleVC.mTitle = self.mTitle
        }
        
        else if segue.identifier != nil && segue.identifier! == "trailerseque" {
            var infoVC = segue.destinationViewController as TrailerController
            
            infoVC.mTitle = self.mTitle
        }
    }*/

}

