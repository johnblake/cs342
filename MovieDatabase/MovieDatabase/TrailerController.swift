//
//  TrailerController.swift
//  MovieDatabase
//
//  Created by Cody Bohlman on 4/19/15.
//  Copyright (c) 2015 Carleton. All rights reserved.
//

import UIKit


class TrailerController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webview: UIWebView?
    
    var URLPath = "https://www.youtube.com/results?search_query="
    var mTitle: String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadAddressURL()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadAddressURL() {
        let requestURL = NSURL(string:URLPath)
        let request = NSURLRequest(URL: requestURL!)
        webview?.loadRequest(request)
    }
    
    func cleanTitle(title: String?) {
        let myReplacementString = String(map(title!.generate()) {
            $0 == " " ? "+" : $0
            })
        self.mTitle = myReplacementString
        self.URLPath = self.URLPath + self.mTitle!
    }
}
