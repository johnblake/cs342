//
//  ReviewController.swift
//  MovieDatabase
//
//  Created by Cody Bohlman on 4/21/15.
//  Copyright (c) 2015 Carleton. All rights reserved.
//

import UIKit

class ReviewController: UIViewController {
    
    @IBOutlet weak var movieTitle: UILabel?
    
    @IBOutlet weak var movieReview: UILabel?
    
    @IBOutlet weak var movieRating: UILabel?
    
    @IBOutlet weak var movieProgressBar: UIProgressView?
    
    var mTitle: String?
    
    var mReview: String?
    
    var mRating: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setValues()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setValues() {
        self.movieTitle!.text = self.mTitle!
        self.movieReview!.text = self.mReview!
        let prog = Float( Float(self.mRating!) / Float(100))
        println(prog)
        self.movieProgressBar!.setProgress(prog, animated: false)
        self.movieRating!.text = " " + String(self.mRating!) + " / 100"
    }
    
}
